package tech.apsystems.sensors.b.web.rest;

import tech.apsystems.sensors.b.service.RFDataService;
import tech.apsystems.sensors.b.web.rest.errors.BadRequestAlertException;
import tech.apsystems.sensors.b.service.dto.RFDataDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.reactive.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link tech.apsystems.sensors.b.domain.RFData}.
 */
@RestController
@RequestMapping("/api")
public class RFDataResource {

    private final Logger log = LoggerFactory.getLogger(RFDataResource.class);

    private static final String ENTITY_NAME = "sensorbRfData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RFDataService rFDataService;

    public RFDataResource(RFDataService rFDataService) {
        this.rFDataService = rFDataService;
    }

    /**
     * {@code POST  /rf-data} : Create a new rFData.
     *
     * @param rFDataDTO the rFDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new rFDataDTO, or with status {@code 400 (Bad Request)} if the rFData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rf-data")
    public Mono<ResponseEntity<RFDataDTO>> createRFData(@RequestBody RFDataDTO rFDataDTO) throws URISyntaxException {
        log.debug("REST request to save RFData : {}", rFDataDTO);
        if (rFDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new rFData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return rFDataService.save(rFDataDTO)
            .map(result -> {
                try {
                    return ResponseEntity.created(new URI("/api/rf-data/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /rf-data} : Updates an existing rFData.
     *
     * @param rFDataDTO the rFDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated rFDataDTO,
     * or with status {@code 400 (Bad Request)} if the rFDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the rFDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rf-data")
    public Mono<ResponseEntity<RFDataDTO>> updateRFData(@RequestBody RFDataDTO rFDataDTO) throws URISyntaxException {
        log.debug("REST request to update RFData : {}", rFDataDTO);
        if (rFDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        return rFDataService.save(rFDataDTO)
            .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
            .map(result -> ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId()))
                .body(result)
            );
    }

    /**
     * {@code GET  /rf-data} : get all the rFData.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of rFData in body.
     */
    @GetMapping("/rf-data")
    public Mono<ResponseEntity<Flux<RFDataDTO>>> getAllRFData(Pageable pageable, ServerHttpRequest request) {
        log.debug("REST request to get a page of RFData");
        return rFDataService.countAll()
            .map(total -> new PageImpl<>(new ArrayList<>(), pageable, total))
            .map(page -> PaginationUtil.generatePaginationHttpHeaders(UriComponentsBuilder.fromHttpRequest(request), page))
            .map(headers -> ResponseEntity.ok().headers(headers).body(rFDataService.findAll(pageable)));
    }

    /**
     * {@code GET  /rf-data/:id} : get the "id" rFData.
     *
     * @param id the id of the rFDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the rFDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rf-data/{id}")
    public Mono<ResponseEntity<RFDataDTO>> getRFData(@PathVariable String id) {
        log.debug("REST request to get RFData : {}", id);
        Mono<RFDataDTO> rFDataDTO = rFDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(rFDataDTO);
    }

    /**
     * {@code DELETE  /rf-data/:id} : delete the "id" rFData.
     *
     * @param id the id of the rFDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rf-data/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteRFData(@PathVariable String id) {
        log.debug("REST request to delete RFData : {}", id);
        return rFDataService.delete(id)            .map(result -> ResponseEntity.noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build()
        );
    }
}
