/**
 * View Models used by Spring MVC REST controllers.
 */
package tech.apsystems.sensors.b.web.rest.vm;
