package tech.apsystems.sensors.b.repository;

import tech.apsystems.sensors.b.domain.RFData;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * Spring Data MongoDB reactive repository for the RFData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RFDataRepository extends ReactiveMongoRepository<RFData, String> {


    Flux<RFData> findAllBy(Pageable pageable);

}
