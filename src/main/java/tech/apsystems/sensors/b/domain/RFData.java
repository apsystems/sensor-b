package tech.apsystems.sensors.b.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.time.Instant;

/**
 * A RFData.
 */
@Document(collection = "rf_data")
public class RFData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("rssi")
    private Double rssi;

    @Field("altitude")
    private Double altitude;

    @Field("middle_frequency")
    private Double middleFrequency;

    @Field("date")
    private Instant date;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getRssi() {
        return rssi;
    }

    public RFData rssi(Double rssi) {
        this.rssi = rssi;
        return this;
    }

    public void setRssi(Double rssi) {
        this.rssi = rssi;
    }

    public Double getAltitude() {
        return altitude;
    }

    public RFData altitude(Double altitude) {
        this.altitude = altitude;
        return this;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getMiddleFrequency() {
        return middleFrequency;
    }

    public RFData middleFrequency(Double middleFrequency) {
        this.middleFrequency = middleFrequency;
        return this;
    }

    public void setMiddleFrequency(Double middleFrequency) {
        this.middleFrequency = middleFrequency;
    }

    public Instant getDate() {
        return date;
    }

    public RFData date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RFData)) {
            return false;
        }
        return id != null && id.equals(((RFData) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RFData{" +
            "id=" + getId() +
            ", rssi=" + getRssi() +
            ", altitude=" + getAltitude() +
            ", middleFrequency=" + getMiddleFrequency() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
