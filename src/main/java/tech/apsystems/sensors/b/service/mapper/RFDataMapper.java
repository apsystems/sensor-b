package tech.apsystems.sensors.b.service.mapper;


import tech.apsystems.sensors.b.domain.*;
import tech.apsystems.sensors.b.service.dto.RFDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RFData} and its DTO {@link RFDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RFDataMapper extends EntityMapper<RFDataDTO, RFData> {



    default RFData fromId(String id) {
        if (id == null) {
            return null;
        }
        RFData rFData = new RFData();
        rFData.setId(id);
        return rFData;
    }
}
