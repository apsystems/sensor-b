package tech.apsystems.sensors.b.service.dto;

import java.time.Instant;
import java.io.Serializable;

/**
 * A DTO for the {@link tech.apsystems.sensors.b.domain.RFData} entity.
 */
public class RFDataDTO implements Serializable {
    
    private String id;

    private Double rssi;

    private Double altitude;

    private Double middleFrequency;

    private Instant date;

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getRssi() {
        return rssi;
    }

    public void setRssi(Double rssi) {
        this.rssi = rssi;
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public Double getMiddleFrequency() {
        return middleFrequency;
    }

    public void setMiddleFrequency(Double middleFrequency) {
        this.middleFrequency = middleFrequency;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RFDataDTO)) {
            return false;
        }

        return id != null && id.equals(((RFDataDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RFDataDTO{" +
            "id=" + getId() +
            ", rssi=" + getRssi() +
            ", altitude=" + getAltitude() +
            ", middleFrequency=" + getMiddleFrequency() +
            ", date='" + getDate() + "'" +
            "}";
    }
}
