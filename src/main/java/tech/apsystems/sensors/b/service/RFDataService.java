package tech.apsystems.sensors.b.service;

import tech.apsystems.sensors.b.domain.RFData;
import tech.apsystems.sensors.b.repository.RFDataRepository;
import tech.apsystems.sensors.b.service.dto.RFDataDTO;
import tech.apsystems.sensors.b.service.mapper.RFDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * Service Implementation for managing {@link RFData}.
 */
@Service
public class RFDataService {

    private final Logger log = LoggerFactory.getLogger(RFDataService.class);

    private final RFDataRepository rFDataRepository;

    private final RFDataMapper rFDataMapper;

    public RFDataService(RFDataRepository rFDataRepository, RFDataMapper rFDataMapper) {
        this.rFDataRepository = rFDataRepository;
        this.rFDataMapper = rFDataMapper;
    }

    /**
     * Save a rFData.
     *
     * @param rFDataDTO the entity to save.
     * @return the persisted entity.
     */
    public Mono<RFDataDTO> save(RFDataDTO rFDataDTO) {
        log.debug("Request to save RFData : {}", rFDataDTO);
        return rFDataRepository.save(rFDataMapper.toEntity(rFDataDTO))
            .map(rFDataMapper::toDto)
;    }

    /**
     * Get all the rFData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Flux<RFDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RFData");
        return rFDataRepository.findAllBy(pageable)
            .map(rFDataMapper::toDto);
    }


    /**
    * Returns the number of rFData available.
    *
    */
    public Mono<Long> countAll() {
        return rFDataRepository.count();
    }

    /**
     * Get one rFData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Mono<RFDataDTO> findOne(String id) {
        log.debug("Request to get RFData : {}", id);
        return rFDataRepository.findById(id)
            .map(rFDataMapper::toDto);
    }

    /**
     * Delete the rFData by id.
     *
     * @param id the id of the entity.
     */
    public Mono<Void> delete(String id) {
        log.debug("Request to delete RFData : {}", id);
        return rFDataRepository.deleteById(id);    }
}
