package tech.apsystems.sensors.b.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import tech.apsystems.sensors.b.web.rest.TestUtil;

public class RFDataDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RFDataDTO.class);
        RFDataDTO rFDataDTO1 = new RFDataDTO();
        rFDataDTO1.setId("id1");
        RFDataDTO rFDataDTO2 = new RFDataDTO();
        assertThat(rFDataDTO1).isNotEqualTo(rFDataDTO2);
        rFDataDTO2.setId(rFDataDTO1.getId());
        assertThat(rFDataDTO1).isEqualTo(rFDataDTO2);
        rFDataDTO2.setId("id2");
        assertThat(rFDataDTO1).isNotEqualTo(rFDataDTO2);
        rFDataDTO1.setId(null);
        assertThat(rFDataDTO1).isNotEqualTo(rFDataDTO2);
    }
}
