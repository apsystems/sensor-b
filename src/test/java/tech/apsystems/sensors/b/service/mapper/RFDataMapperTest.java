package tech.apsystems.sensors.b.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class RFDataMapperTest {

    private RFDataMapper rFDataMapper;

    @BeforeEach
    public void setUp() {
        rFDataMapper = new RFDataMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        String id = "id1";
        assertThat(rFDataMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(rFDataMapper.fromId(null)).isNull();
    }
}
