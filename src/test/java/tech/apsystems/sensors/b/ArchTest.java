package tech.apsystems.sensors.b;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("tech.apsystems.sensors.b");

        noClasses()
            .that()
                .resideInAnyPackage("tech.apsystems.sensors.b.service..")
            .or()
                .resideInAnyPackage("tech.apsystems.sensors.b.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..tech.apsystems.sensors.b.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
