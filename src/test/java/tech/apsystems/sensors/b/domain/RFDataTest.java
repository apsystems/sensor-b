package tech.apsystems.sensors.b.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import tech.apsystems.sensors.b.web.rest.TestUtil;

public class RFDataTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RFData.class);
        RFData rFData1 = new RFData();
        rFData1.setId("id1");
        RFData rFData2 = new RFData();
        rFData2.setId(rFData1.getId());
        assertThat(rFData1).isEqualTo(rFData2);
        rFData2.setId("id2");
        assertThat(rFData1).isNotEqualTo(rFData2);
        rFData1.setId(null);
        assertThat(rFData1).isNotEqualTo(rFData2);
    }
}
