package tech.apsystems.sensors.b.web.rest;

import tech.apsystems.sensors.b.SensorbApp;
import tech.apsystems.sensors.b.config.TestSecurityConfiguration;
import tech.apsystems.sensors.b.domain.RFData;
import tech.apsystems.sensors.b.repository.RFDataRepository;
import tech.apsystems.sensors.b.service.RFDataService;
import tech.apsystems.sensors.b.service.dto.RFDataDTO;
import tech.apsystems.sensors.b.service.mapper.RFDataMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.springframework.security.test.web.reactive.server.SecurityMockServerConfigurers.csrf;

/**
 * Integration tests for the {@link RFDataResource} REST controller.
 */
@SpringBootTest(classes = { SensorbApp.class, TestSecurityConfiguration.class })
@AutoConfigureWebTestClient
@WithMockUser
public class RFDataResourceIT {

    private static final Double DEFAULT_RSSI = 1D;
    private static final Double UPDATED_RSSI = 2D;

    private static final Double DEFAULT_ALTITUDE = 1D;
    private static final Double UPDATED_ALTITUDE = 2D;

    private static final Double DEFAULT_MIDDLE_FREQUENCY = 1D;
    private static final Double UPDATED_MIDDLE_FREQUENCY = 2D;

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private RFDataRepository rFDataRepository;

    @Autowired
    private RFDataMapper rFDataMapper;

    @Autowired
    private RFDataService rFDataService;

    @Autowired
    private WebTestClient webTestClient;

    private RFData rFData;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RFData createEntity() {
        RFData rFData = new RFData()
            .rssi(DEFAULT_RSSI)
            .altitude(DEFAULT_ALTITUDE)
            .middleFrequency(DEFAULT_MIDDLE_FREQUENCY)
            .date(DEFAULT_DATE);
        return rFData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RFData createUpdatedEntity() {
        RFData rFData = new RFData()
            .rssi(UPDATED_RSSI)
            .altitude(UPDATED_ALTITUDE)
            .middleFrequency(UPDATED_MIDDLE_FREQUENCY)
            .date(UPDATED_DATE);
        return rFData;
    }

    @BeforeEach
    public void setupCsrf() {
        webTestClient = webTestClient.mutateWith(csrf());
    }

    @BeforeEach
    public void initTest() {
        rFDataRepository.deleteAll().block();
        rFData = createEntity();
    }

    @Test
    public void createRFData() throws Exception {
        int databaseSizeBeforeCreate = rFDataRepository.findAll().collectList().block().size();
        // Create the RFData
        RFDataDTO rFDataDTO = rFDataMapper.toDto(rFData);
        webTestClient.post().uri("/api/rf-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(rFDataDTO))
            .exchange()
            .expectStatus().isCreated();

        // Validate the RFData in the database
        List<RFData> rFDataList = rFDataRepository.findAll().collectList().block();
        assertThat(rFDataList).hasSize(databaseSizeBeforeCreate + 1);
        RFData testRFData = rFDataList.get(rFDataList.size() - 1);
        assertThat(testRFData.getRssi()).isEqualTo(DEFAULT_RSSI);
        assertThat(testRFData.getAltitude()).isEqualTo(DEFAULT_ALTITUDE);
        assertThat(testRFData.getMiddleFrequency()).isEqualTo(DEFAULT_MIDDLE_FREQUENCY);
        assertThat(testRFData.getDate()).isEqualTo(DEFAULT_DATE);
    }

    @Test
    public void createRFDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = rFDataRepository.findAll().collectList().block().size();

        // Create the RFData with an existing ID
        rFData.setId("existing_id");
        RFDataDTO rFDataDTO = rFDataMapper.toDto(rFData);

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient.post().uri("/api/rf-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(rFDataDTO))
            .exchange()
            .expectStatus().isBadRequest();

        // Validate the RFData in the database
        List<RFData> rFDataList = rFDataRepository.findAll().collectList().block();
        assertThat(rFDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllRFData() {
        // Initialize the database
        rFDataRepository.save(rFData).block();

        // Get all the rFDataList
        webTestClient.get().uri("/api/rf-data?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id").value(hasItem(rFData.getId()))
            .jsonPath("$.[*].rssi").value(hasItem(DEFAULT_RSSI.doubleValue()))
            .jsonPath("$.[*].altitude").value(hasItem(DEFAULT_ALTITUDE.doubleValue()))
            .jsonPath("$.[*].middleFrequency").value(hasItem(DEFAULT_MIDDLE_FREQUENCY.doubleValue()))
            .jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString()));
    }
    
    @Test
    public void getRFData() {
        // Initialize the database
        rFDataRepository.save(rFData).block();

        // Get the rFData
        webTestClient.get().uri("/api/rf-data/{id}", rFData.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk()
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id").value(is(rFData.getId()))
            .jsonPath("$.rssi").value(is(DEFAULT_RSSI.doubleValue()))
            .jsonPath("$.altitude").value(is(DEFAULT_ALTITUDE.doubleValue()))
            .jsonPath("$.middleFrequency").value(is(DEFAULT_MIDDLE_FREQUENCY.doubleValue()))
            .jsonPath("$.date").value(is(DEFAULT_DATE.toString()));
    }
    @Test
    public void getNonExistingRFData() {
        // Get the rFData
        webTestClient.get().uri("/api/rf-data/{id}", Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound();
    }

    @Test
    public void updateRFData() throws Exception {
        // Initialize the database
        rFDataRepository.save(rFData).block();

        int databaseSizeBeforeUpdate = rFDataRepository.findAll().collectList().block().size();

        // Update the rFData
        RFData updatedRFData = rFDataRepository.findById(rFData.getId()).block();
        updatedRFData
            .rssi(UPDATED_RSSI)
            .altitude(UPDATED_ALTITUDE)
            .middleFrequency(UPDATED_MIDDLE_FREQUENCY)
            .date(UPDATED_DATE);
        RFDataDTO rFDataDTO = rFDataMapper.toDto(updatedRFData);

        webTestClient.put().uri("/api/rf-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(rFDataDTO))
            .exchange()
            .expectStatus().isOk();

        // Validate the RFData in the database
        List<RFData> rFDataList = rFDataRepository.findAll().collectList().block();
        assertThat(rFDataList).hasSize(databaseSizeBeforeUpdate);
        RFData testRFData = rFDataList.get(rFDataList.size() - 1);
        assertThat(testRFData.getRssi()).isEqualTo(UPDATED_RSSI);
        assertThat(testRFData.getAltitude()).isEqualTo(UPDATED_ALTITUDE);
        assertThat(testRFData.getMiddleFrequency()).isEqualTo(UPDATED_MIDDLE_FREQUENCY);
        assertThat(testRFData.getDate()).isEqualTo(UPDATED_DATE);
    }

    @Test
    public void updateNonExistingRFData() throws Exception {
        int databaseSizeBeforeUpdate = rFDataRepository.findAll().collectList().block().size();

        // Create the RFData
        RFDataDTO rFDataDTO = rFDataMapper.toDto(rFData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient.put().uri("/api/rf-data")
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(rFDataDTO))
            .exchange()
            .expectStatus().isBadRequest();

        // Validate the RFData in the database
        List<RFData> rFDataList = rFDataRepository.findAll().collectList().block();
        assertThat(rFDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteRFData() {
        // Initialize the database
        rFDataRepository.save(rFData).block();

        int databaseSizeBeforeDelete = rFDataRepository.findAll().collectList().block().size();

        // Delete the rFData
        webTestClient.delete().uri("/api/rf-data/{id}", rFData.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNoContent();

        // Validate the database contains one less item
        List<RFData> rFDataList = rFDataRepository.findAll().collectList().block();
        assertThat(rFDataList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
